var gulp = require('gulp'),
	browserSync = require('browser-sync'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	plumber = require('gulp-plumber'),
	replace = require('gulp-replace'),
	bump = require('gulp-bump'),

	// Html =================================================
	htmlreplace = require('gulp-html-replace'),
	templateCache = require('gulp-angular-templatecache'),
	htmlify = require('gulp-angular-htmlify'),
	minifyHTML = require('gulp-minify-html'),

	// CSS ==================================================
	sass = require('gulp-sass'),
	minifyCSS = require('gulp-clean-css'),
	autoprefixer = require('gulp-autoprefixer'),

	// JS ==================================================
	ngAnnotate = require('gulp-ng-annotate'),
	uglify = require('gulp-uglify'),

	// Images ================================================
	imageop = require('gulp-imagemin'),

	// Author Signature =======================================
	header = require('gulp-header'),
	packageFile = require('./package.json'),
	appName = 'varo',
	version = '0.0.1',
	headerComment = [
		'/*!\n' +
		' * <%= package.name %>\n' +
		' * @version ' + version + '\n' +
		' */',
		'\n'
	].join('');

gulp.task('bump', function () {
	gulp.src(['./bower.json', './package.json', './config.xml'])
		.pipe(bump({
			version: version
		}))
		.pipe(gulp.dest('./'));
});

gulp.task('index', function () {
	// move html partials to different platforms
	gulp.src('dev/index.html')
		.pipe(htmlreplace({
			'css': 'css/style.min.css',
			'libs': 'libs/libs.min.js',
			'js': 'js/app.min.js',
			'templates': 'templates/templates.js',
			'partials': 'partials/partials.js'
		}))
		.pipe(htmlify({
			customPrefixes: ['ui-', 'ng-']
		}))
		.pipe(minifyHTML({
			empty: true,
			conditionals: true,
			quotes: true
		}))
		.pipe(gulp.dest('www/'))
});

gulp.task('partials', function () {
	// move html partials to different platforms
	gulp.src(['dev/partials/*.html', 'dev/partials/**/*.html'])
		.pipe(htmlify({
			customPrefixes: ['ui-', 'ng-']
		}))
		.pipe(templateCache({
			filename: 'partials.js',
			root: 'partials/',
			module: appName,
			standalone: false
		}))
		.pipe(gulp.dest('www/partials/'))
});

gulp.task('templates', function () {
	// move html templates to different platforms
	gulp.src(['dev/templates/*.html', 'dev/templates/**/*.html'])
		.pipe(htmlify({
			customPrefixes: ['ui-', 'ng-']
		}))
		.pipe(templateCache({
			filename: 'templates.js',
			root: 'templates/',
			module: appName,
			standalone: false
		}))
		.pipe(gulp.dest('www/templates/'))
});

gulp.task('scss', function () {
	// create css using css and place in the css directory
	gulp.src(['dev/scss/*.scss', 'dev/scss/**/*.scss'])
		.pipe(plumber())
		.pipe(sass())
		.pipe(concat('style.css'))
		/*.pipe(autoprefixer({
			browsers: ['last 4 versions'],
			cascade: false
		}))*/
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(minifyCSS())
		.pipe(header(headerComment, {
			package: packageFile
		}))
		.pipe(gulp.dest('www/css/'))
});

gulp.task('libs', function () {
	gulp.src([
			'dev/bower_components/swiper/dist/js/swiper.min.js',
			'dev/bower_components/qrcode-generator/js/qrcode.js',
			'dev/bower_components/qrcode-generator/js/qrcode_UTF8.js',

			'dev/bower_components/angular/angular.min.js',
			'dev/bower_components/angular-ui-router/release/angular-ui-router.min.js',
			'dev/bower_components/angular-touch/angular-touch.min.js',
			'dev/bower_components/angular-animate/angular-animate.min.js',
			'dev/bower_components/angular-aria/angular-aria.min.js',
			'dev/bower_components/angular-messages/angular-messages.min.js',
			'dev/bower_components/angular-sanitize/angular-sanitize.min.js',
			'dev/bower_components/angular-material/angular-material.js',
			'dev/bower_components/angular-cookies/angular-cookies.min.js',
			'dev/bower_components/angular-resource/angular-resource.min.js',
			
			'dev/bower_components/ngCordova/dist/ng-cordova.min.js',
			'dev/bower_components/angular-socket-io/socket.min.js',
			'dev/bower_components/angular-swiper/dist/angular-swiper.js',
			'dev/bower_components/angular-qrcode/angular-qrcode.js',
			'dev/bower_components/ngmap/build/scripts/ng-map.min.js'
		])
		.pipe(plumber())
		.pipe(concat('libs.js'))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(uglify())
		.pipe(header(headerComment, {
			package: packageFile
		}))
		.pipe(gulp.dest('www/libs/'))
});

gulp.task('scripts', function () {
	gulp.src([
			'dev/js/*.js',
			'dev/js/**/*.js'
		])
		.pipe(plumber())
		.pipe(concat('app.js'))
		.pipe(replace('version_num', version))
		.pipe(ngAnnotate())
		.pipe(rename({
			suffix: '.min'
		}))
		//.pipe(uglify())
		.pipe(header(headerComment, {
			package: packageFile
		}))
		.pipe(gulp.dest('www/js/'))
});

gulp.task('images', function () {
	gulp.src(['dev/images/**/*.png', 'dev/images/**/*.jpg', 'dev/images/**/*.gif', 'dev/images/**/*.jpeg', 'dev/images/**/*.svg'])
		.pipe(imageop())
		.pipe(gulp.dest('www/images'))
});

gulp.task('fonts', function () {
	// move fonts to different platforms
	gulp.src('dev/fonts/**')
		.pipe(gulp.dest('www/fonts/'))
});

// gulp.task('videos', function () {
//     // move fonts to different platforms
//     gulp.src('dev/videos/**')
//         .pipe(gulp.dest('www/videos/'))
// });

gulp.task('browser-sync', function () {
	browserSync.init(['www/**'], {
		//port: 8008,
		server: {
			baseDir: './www'
		}
	});
});

gulp.task('watch-scss', function () {
	gulp.watch(['dev/scss/*.scss', 'dev/scss/**/*.scss'], ['scss']);
});

gulp.task('watch-js', function () {
	gulp.watch(['dev/js/**'], ['scripts']);
});

gulp.task('watch-index', function () {
	gulp.watch(['dev/*.html'], ['index']);
});

gulp.task('watch-partials', function () {
	gulp.watch(['dev/partials/*.html', 'dev/partials/**/*.html'], ['partials']);
});

gulp.task('watch-templates', function () {
	gulp.watch(['dev/templates/*.html', 'dev/templates/**/*.html'], ['templates']);
});

gulp.task('watch-images', function () {
	gulp.watch(['dev/images/**'], ['images']);
});

// Build
gulp.task('build', ['bump', 'index', 'partials', 'templates', 'images', 'fonts', 'scss', 'libs', 'scripts' /*, 'data'*/ ]);

// DEFAULT TASK with browser-sync
gulp.task('default', ['build'], function () {
	gulp.start('browser-sync', 'watch-scss', 'watch-js', 'watch-index', 'watch-partials', 'watch-templates', 'watch-images');
});