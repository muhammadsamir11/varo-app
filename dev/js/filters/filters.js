'use strict';
angular.module(_FILTERS_)

    .filter('trustUrl', function ($sce) {
        return function (recordingUrl) {
            return $sce.trustAs('resourceUrl', recordingUrl);
        };
    });