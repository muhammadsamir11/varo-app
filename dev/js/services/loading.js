'use strict';
angular.module(_SERVICES_)

  .factory('$loading', function ($q, $mdToast, $cordovaSpinnerDialog) {
    return {
      show: function (message, persistent) {
        if (_IS_MOBILE_) {
          $cordovaSpinnerDialog.show('', message || '', persistent || false);
        } else {
          $mdToast.show(
            $mdToast.simple()
            .textContent(message || '')
            .hideDelay(3000)
          );
        }
      },
      hide: function () {
        if (_IS_MOBILE_) {
          $cordovaSpinnerDialog.hide();
        } else {
          $mdToast.hide();
        }
      }
    };
  });