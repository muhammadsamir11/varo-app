'use strict';
angular.module(_SERVICES_)

  .factory('authService', function authService(Customer, $rootScope) {
    var service = {
      login: login,
      logout: logout,
      register: register,
      isAuthenticated: isAuthenticated,
      getCurrentCustomer: getCurrentCustomer
    };
    return service;

    function login(username, password) {
      return Customer
        .login({
          username: username,
          password: password
        })
        .$promise;
    }

    function logout() {
      return Customer
        .logout()
        .$promise;
    }

    function register(userData) {
      return Customer
        .create({
          username: userData.mobile,
          mobile: userData.mobile,
          fname: userData.fname,
          lname: userData.lname,
          faceId: userData.faceId,
          photo: userData.photo,
          password: userData.password
        })
        .$promise;
    }

    function isAuthenticated() {
      return Customer.isAuthenticated();
    }

    function getCurrentCustomer() {
      return Customer
        .getCurrent()
        .$promise;
    }
  });