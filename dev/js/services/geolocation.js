'use strict';
angular.module(_SERVICES_)

    .factory('geolocation', function ($q, $cordovaGeolocation) {
        var cachedGeolocation = null;
        return {
            get: function () {
                var deferred = $q.defer();
                if (cachedGeolocation) {
                    if (_DEBUG_) console.log('Cached Geolocation', cachedGeolocation);
                    deferred.resolve(cachedGeolocation);
                } else {
                    if (_DEBUG_) console.log('Caching Geolocation', cachedGeolocation);
                    $cordovaGeolocation
                        .getCurrentPosition({
                            timeout: 10000,
                            enableHighAccuracy: false,
                            priority: 102
                        })
                        .then(function (position) {
                            cachedGeolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            deferred.resolve(cachedGeolocation);
                        }, function (err) {
                            if (_DEBUG_) console.log(err);
                            deferred.resolve(null);
                        });
                }
                return deferred.promise;
            }
        };
    });