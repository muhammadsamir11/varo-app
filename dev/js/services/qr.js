'use strict';
angular.module(_SERVICES_)

  .factory('$cordovaQr', function ($q, $window) {
    return {
      prepare: function () {
        var q = $q.defer();

        if (_IS_MOBILE_) {
          QRScanner.prepare(function onDone(err, status) {
            if (err) {
              q.reject(error);
              // here we can handle errors and clean up any loose ends.
              console.error(err);
            }
            if (status.authorized) {
              q.resolve(status);
              // W00t, you have camera access and the scanner is initialized.
              // QRscanner.show() should feel very fast.
            } else if (status.denied) {
              // The video preview will remain black, and scanning is disabled. We can
              // try to ask the user to change their mind, but we'll have to send them
              // to their device settings with `QRScanner.openSettings()`.
            } else {
              // we didn't get permission, but we didn't get permanently denied. (On
              // Android, a denial isn't permanent unless the user checks the "Don't
              // ask again" box.) We can ask again at the next relevant opportunity.
            }
          });
        } else {
          q.reject('QR Prepare');
        }

        return q.promise;
      },

      show: function () {
        if (_IS_MOBILE_) {
          QRScanner.show();
        } else {
          console.log('QR Show');
        }
      },

      scan: function () {
        var q = $q.defer();
        
        if (_IS_MOBILE_) {
          QRScanner.scan(function (err, text) {
            if (err) {
              q.reject(err);
            } else {
              q.resolve(text);
            }
          });
        } else {
          q.reject('QR Scan');
        }

        return q.promise;
      },

      cancel: function () {
        if (_IS_MOBILE_) {
          QRScanner.cancelScan();
        } else {
          console.log('QR Cancel');
        }
      },

      destroy: function () {
        if (_IS_MOBILE_) {
          QRScanner.destroy(function (status) {
            console.log(status);
          });
        } else {
          console.log('QR Destroy');
        }
      }
    };
  });