'use strict';
angular.module(_SERVICES_)

  .factory('LoopBackAuth', function () {
    var props = ['accessTokenId', 'currentUserId'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function (name) {
        self[name] = load(name);
      });
      this.rememberMe = undefined;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function () {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function (name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function (accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    }

    LoopBackAuth.prototype.clearUser = function () {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.clearStorage = function () {
      props.forEach(function (name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      var key = propsPrefix + name;
      if (value == null) value = '';
      storage[key] = value;
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })


  .factory('LoopBackAuthRequestInterceptor', function ($q, LoopBackAuth) {
    return {
      'request': function (config) {
        if (LoopBackAuth.accessTokenId) {
          config.headers.authorization = LoopBackAuth.accessTokenId;
        } else if (config.__isGetCurrentUser__) {
          // Return a stub 401 error for User.getCurrent() when
          // there is no user logged in
          var res = {
            body: {
              error: {
                status: 401
              }
            },
            status: 401,
            config: config,
            headers: function () {
              return undefined;
            }
          };
          return $q.reject(res);
        }
        return config || $q.when(config);
      }
    }
  })

  
  .factory('LoopBackResource', function ($resource) {
    return function (url, params, actions) {
      var resource = $resource(url, params, actions);

      // Angular always calls POST on $save()
      // This hack is based on
      // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
      resource.prototype.$save = function (success, error) {
        // Fortunately, LoopBack provides a convenient `upsert` method
        // that exactly fits our needs.
        var result = resource.upsert.call(this, {}, this, success, error);
        return result.$promise || result;
      }

      return resource;
    };
  });