'use strict';
angular.module(_SERVICES_)

  .factory('$cordovaCameraPreview', function ($q, $window, $timeout) {
    return {
      start: function (x, y, w, h, d) {
        if (_IS_MOBILE_) {
          let options = {
            x: x || 0,
            y: y || ($window.screen.height / 2) - ((($window.screen.height / 2) / 2) + 10),
            width: w || $window.screen.width,
            height: h || $window.screen.height / 2,
            camera: d == 'front' ? CameraPreview.CAMERA_DIRECTION.FRONT : CameraPreview.CAMERA_DIRECTION.BACK,
            toBack: true,
            tapPhoto: false,
            tapFocus: false,
            previewDrag: false
          };
          $window.CameraPreview.startCamera(options);
        } else {
          console.log('Start Camera');
        }
      },
      stop: function () {
        if (_IS_MOBILE_) {
          $window.CameraPreview.stopCamera();
        } else {
          console.log('Stop Camera');
        }
      },
      takePicture: function () {
        var q = $q.defer();

        if (_IS_MOBILE_) {
          $window.CameraPreview.takePicture({
            width: 640,
            height: 640,
            quality: 55
          }, function (base64PictureData) {
            q.resolve(base64PictureData);
          });
        } else {
          q.reject('Take Picture');
        }

        return q.promise;
      }
    };
  });