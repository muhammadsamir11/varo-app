'use strict';
angular.module(_SERVICES_)

  .factory('$cordovaFingerprint', function ($q, $window) {
    return {
      isAvailable: function () {
        var q = $q.defer();

        if (typeof $window.FingerprintAuth != 'undefined') {
          $window.FingerprintAuth.isAvailable(isAvailableSuccess, isAvailableError);

          function isAvailableSuccess(result) {
            q.resolve(result);
          }

          function isAvailableError(err) {
            q.reject(err);
          }
        }

        return q.promise;
      },

      encrypt: function (username, password) {
        var q = $q.defer();

        $window.FingerprintAuth.encrypt({
          clientId: "varo",
          username: username,
          password: password
        }, successCallback, errorCallback);

        function successCallback(result) {
          q.resolve(result);

          console.log("successCallback(): " + JSON.stringify(result));
          if (result.withFingerprint) {
            console.log("Successfully encrypted credentials.");
            console.log("Encrypted credentials: " + result.token);
          } else if (result.withBackup) {
            console.log("Authenticated with backup password");
          }
        }

        function errorCallback(err) {
          q.reject(err);
        }

        return q.promise;
      },

      decrypt: function (username, token) {
        var q = $q.defer();

        $window.FingerprintAuth.decrypt({
          clientId: "varo",
          username: username,
          token: token
        }, successCallback, errorCallback);

        function successCallback(result) {
          q.resolve(result);
          console.log("successCallback(): " + JSON.stringify(result));

          if (result.withFingerprint) {
            console.log("Successful biometric authentication.");
          } else if (result.withBackup) {
            console.log("Authenticated with backup password");
          }
        }

        function errorCallback(err) {
          q.reject(err);
        }

        return q.promise;
      },

      delete: function (username) {
        var q = $q.defer();

        $window.FingerprintAuth.delete({
          clientId: "varo",
          username: username
        }, successCallback, errorCallback);

        function successCallback(result) {
          q.resolve(result);
        }

        function errorCallback(err) {
          q.reject(err);
        }

        return q.promise;
      }

    };
  });