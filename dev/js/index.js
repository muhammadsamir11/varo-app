var _INDEX_ = {
    initialize: function () {
        this.bindEvents();
    },
    bindEvents: function () {
        if (typeof (cordova) !== 'undefined' || typeof (phonegap) !== 'undefined') {
            // Deviceready detection.
            document.addEventListener('deviceready', _INDEX_.onDeviceReady, false);
            _IS_MOBILE_ = true;
        } else {
            window.onload = _INDEX_.onDeviceReady();
            _IS_MOBILE_ = false;
        }
    },
    onDeviceReady: function () {
        _INDEX_.receivedEvent('deviceready');

        // Background detection.
        document.addEventListener('pause', _INDEX_.onPause, false);
        document.addEventListener('resume', _INDEX_.onResume, false);
    },
    onPause: function () {
        _IN_BACKGROUND_ = true;
        if (_DEBUG_) console.log('_IN_BACKGROUND_ = true');
    },
    onResume: function () {
        setTimeout(function () {
            _IN_BACKGROUND_ = false;
            if (_DEBUG_) console.log('_IN_BACKGROUND_ = false');
        }, 0);
    },
    receivedEvent: function (event) {
        switch (event) {
            case 'deviceready':
                _INDEX_.initMainView();
                break;
        }
    },
    initMainView: function () {
        // START ANGULAR
        angular.element(document).ready(function () {
            angular.bootstrap(document, [_APP_]);
        });
    }
};

_INDEX_.initialize();