'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardCtrl', function ($scope, authService) {
    if (_DEBUG_) console.log('DashboardCtrl ====');

    // Customer
    $scope.customer = null;
    authService.getCurrentCustomer()
      .then(function (customer) {
        $scope.customer = customer;
      });
  });