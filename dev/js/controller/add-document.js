'use strict';
angular.module(_CONTROLLERS_)

  .controller('AddDocumentCtrl', function ($scope, $state, $stateParams, $http, $loading, $cordovaCameraPreview, authService, apiEndpoint) {
    if (_DEBUG_) console.log('AddDocumentCtrl ====');

    // Camera
    $cordovaCameraPreview.start(null, null, null, null, null);

    // Steps
    $scope.steps = [];
    $http({
        method: 'GET',
        url: apiEndpoint + '/Types/' + $stateParams.id
      })
      .then(function mySuccess(response) {
        $loading.hide();
        $scope.steps = response.data.steps;
      }, function myError(err) {
        console.log(err);
        $loading.hide();
      });

    // Data
    $scope.documentData = {
      photos: []
    };

    // Take Photo
    $scope.takePhoto = function (steps) {
      $loading.show('Loading', false);

      $cordovaCameraPreview.takePicture()
        .then(function (base64) {
          $loading.hide();

          $scope.documentData.photos.push(base64);
          if ($scope.documentData.photos.length === steps.length) {
            $scope.addDocument();
          }
          // Tesseract
          // $cordovaTesseract.recognizeText(base64, 'eng')
          //   .then(function (recognizedText) {
          //     $loading.hide();
          //     console.log(recognizedText);
          //   })
          //   .catch(function (reason) {
          //     $loading.hide();
          //     console.log(reason);
          //   });
        })
        .catch(function (err) {
          $loading.hide();

          // Testing
          $scope.documentData.photos.push('base64');
          if ($scope.documentData.photos.length === steps.length) {
            $scope.addDocument();
          }
        });
    };

    // Post
    $scope.addDocument = function () {
      $loading.show('Loading', false);

      authService.getCurrentCustomer()
        .then(function (customer) {
          $http({
              method: 'POST',
              url: apiEndpoint + '/Documents',
              data: {
                customerId: customer.id,
                typeId: $stateParams.id,
                photos: $scope.documentData.photos
              }
            })
            .then(function mySuccess(response) {
              $loading.hide();

              $state.go('app.dashboard.documents', null, {
                location: 'replace',
                reload: true
              });
            }, function myError(err) {
              console.log(err);
              $loading.hide();
            });
        });
    };

    $scope.$on("$destroy", function () {
      $cordovaCameraPreview.stop();
    });
  });