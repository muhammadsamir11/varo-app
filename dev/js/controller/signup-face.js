'use strict';
angular.module(_CONTROLLERS_)

  .controller('SignupFaceCtrl', function ($scope, $state, $http, $timeout, $loading, $cordovaCameraPreview, faceApi) {
    if (_DEBUG_) console.log('SignupFaceCtrl ====');

    // ezar.initializeVideoOverlay(function () {
    //   //setTimeout(function () {
    //     ezar.getFrontCamera().start(function () {
    //       //ezar.watchFaces(onFaces, err);
    //     }, err);
    //   //}, 1500);
    // }, err);

    // function onFaces(faces) {
    //   console.log(faces);
    // };

    // function err(msg) {
    //   alert('Error: ' + msg);
    // };

    // Camera
    $cordovaCameraPreview.start(null, null, null, null, 'front');

    // Take Photo
    $scope.takePhoto = function () {
      $loading.show('Loading', false);

      $http({
          method: 'POST',
          url: faceApi.endpoint + '/persongroups/' + faceApi.group + '/persons',
          headers: {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': faceApi.key
          },
          data: {
            //$scope.userData.mobile.substr($scope.userData.mobile.length - 4)
            'name': 'Person-',
            'userData': null
          }
        })
        .then(function (response) {
          $cordovaCameraPreview.takePicture()
            .then(function (base64) {
              $http({
                  method: 'POST',
                  url: faceApi.endpoint + '/persongroups/' + faceApi.group + '/persons/' + response.data.personId + '/persistedFaces',
                  headers: {
                    'Content-Type': 'application/octet-stream',
                    'Ocp-Apim-Subscription-Key': faceApi.key
                  },
                  data: b64toBlob(base64)
                })
                .then(function (response) {
                  $loading.hide();
                  $scope.userData.faceId = response.data.persistedFaceId;
                  $scope.userData.photo = base64;

                  $state.go('app.signup.password');
                }, function (err) {
                  if (_DEBUG_) console.log(err.data.error.message);
                  $loading.hide();

                  alert('Sorry, I cant find any face :)');
                });
            })
            .catch(function (err) {
              if (_DEBUG_) console.log(err);

              // Testing
              $scope.userData.faceId = '123-123-123';
              $scope.userData.photo = 'base64';

              $state.go('app.signup.password');
            });
        }, function (err) {
          if (_DEBUG_) console.log(err.data.error.message);
        });
    };

    $scope.$on('$destroy', function () {
      $cordovaCameraPreview.stop();
    });

    var b64toBlob = function (base64) {
      var contentType = 'image/png';
      var byteCharacters = atob(base64);
      var byteNumbers = new Array(byteCharacters.length);
      for (var i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      var blob = new Blob([byteArray], {
        type: contentType
      });
      return blob;
    };
  });