'use strict';
angular.module(_CONTROLLERS_)

  .controller('MainCtrl', function ($scope, $state, $stateParams, $timeout, $cordovaDialogs, $cordovaSplashscreen) {
    if (_DEBUG_) console.log('MainCtrl ====');

    // Global
    $scope.$state = $state;
    $scope.$stateParams = $stateParams;

    // Go
    $scope.go = function (state) {
      $state.go(state, null, {
        location: 'replace',
        reload: true
      });
    };

    // Back
    $scope.back = function () {
      if ($state.current.name !== 'app.home' || $state.current.name !== 'app.login') {
        if (_IS_MOBILE_) {
          history.go(-1);
          //navigator.app.backHistory();
        } else {
          window.history.back();
        }
      }
    };

    // Cancel
    $scope.cancel = function (state) {
      $timeout(function () {
        $state.go(state, null, {
          location: 'replace',
          reload: true
        });
      }, 100);
    };

    // Open url
    $scope.openUrl = function (url) {
      if (_DEBUG_) console.log(url);
      if (_IS_MOBILE_) navigator.app.loadUrl(url, {
        openExternal: true
      });
      else window.open(url, '_system');
    };

    // Soon
    $scope.soon = function () {
      $cordovaDialogs.alert('Not available now!', 'Sorry', 'ok')
        .then(function () {});
    };

    // Hide Splash
    $timeout(function () {
      if (_IS_MOBILE_) $cordovaSplashscreen.hide();
    }, 500);
  });