'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardRequestCtrl', function ($scope, $state, $stateParams, $timeout, $http, $loading, $mdDialog, apiEndpoint, authService) {
    if (_DEBUG_) console.log('DashboardRequestCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Request
    $scope.request = null;
    $http({
        method: 'GET',
        url: apiEndpoint + '/Requests/' + $stateParams.id
      })
      .then(function mySuccess(response) {
          $loading.hide();
          $scope.request = response.data;

          // Check Payment
          if (!response.data.payment && response.data.product.payment) $scope.showPayment();
        },
        function myError(err) {
          console.log(err);
          $loading.hide();
        });

    // Remove Request
    $scope.remove = function () {
      $loading.show('Loading', false);

      $http({
          method: 'DELETE',
          url: apiEndpoint + '/Requests/' + $stateParams.id
        })
        .then(function mySuccess(response) {
          $loading.hide();
          $state.go('app.dashboard.requests', null, {
            location: 'replace',
            reload: true
          });
        }, function myError(err) {
          console.log(err);
          $loading.hide();
        });
    };

    // Show Payment
    $scope.showPayment = function (ev) {
      $mdDialog.show({
        contentElement: '#paymentDialog',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      });
    };

    // Hide Payment
    $scope.hidePayment = function () {
      $mdDialog.hide();
    };
  });