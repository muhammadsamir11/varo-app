'use strict';
angular.module(_CONTROLLERS_)

  .controller('AddTypesCtrl', function ($scope, $state, $http, $loading, apiEndpoint) {
    if (_DEBUG_) console.log('AddTypesCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Types
    $scope.types = [];
    $http({
        method: 'GET',
        url: apiEndpoint + '/Types'
      })
      .then(function mySuccess(response) {
        $loading.hide();
        $scope.types = response.data;
      }, function myError(err) {
        console.log(err);
        $loading.hide();
      });
  });