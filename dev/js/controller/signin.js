'use strict';
angular.module(_CONTROLLERS_)

  .controller('SigninCtrl', function ($scope, $state, $location, $http, $loading, authService) {
    if (_DEBUG_) console.log('SigninCtrl ====');

    // Signin
    $scope.signin = function (username, password) {
      $loading.show('Loading', false);

      authService.login(username, password)
        .then(function (response) {
          console.log(response);
          //$http.defaults.headers.common['authorization'] = response.id;

          $loading.hide();
          $location.path('/dashboard');
          // $state.go('app.dashboard.home', null, {
          //   location: 'replace',
          //   reload: true
          // });
        }, function (err) {
          $loading.hide();
          
          alert(err.data.error.message);
          console.log(err);
        });
    };



    // Fingerprint
    // $scope.available = "Not checked";
    // if (_IS_MOBILE_) {
    //   $cordovaFingerprint.isAvailable()
    //     .then(function (result) {
    //       $scope.available = "Fingerprint available";

    //       $cordovaFingerprint.encrypt('username', 'password')
    //         .then(function (result) {
    //           alert(result.token);
    //         })
    //         .catch(function (err) {
    //         });
    //     })
    //     .catch(function (err) {
    //       $scope.available = "isAvailableError(): " + JSON.stringify(err);
    //     });
    // }
  });