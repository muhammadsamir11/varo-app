'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardSettingsCtrl', function ($scope, $location, $loading, authService) {
    if (_DEBUG_) console.log('DashboardSettingsCtrl ====');

    // Logout
    $scope.logout = function () {
      authService.logout()
        .then(function (response) {
          $location.path('/signin');
          console.log(response);
        }, function (err) {
          console.log(err);
        });
    };
  });