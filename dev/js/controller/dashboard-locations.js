'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardLocationsCtrl', function ($scope, $state, $http, $loading, authService, apiEndpoint) {
    if (_DEBUG_) console.log('DashboardLocationsCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Agent
    $scope.agents = null;
    $http({
        method: 'GET',
        url: apiEndpoint + '/Agents'
      })
      .then(function mySuccess(agents) {
        $loading.hide();
        $scope.agents = agents.data;
      }, function myError(err) {
        console.log(err);
        $loading.hide();
      });
  });