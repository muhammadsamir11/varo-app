'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardDocumentCtrl', function ($scope, $state, $stateParams, $timeout, $http, $loading, $mdDialog, apiEndpoint, authService) {
    if (_DEBUG_) console.log('DashboardDocumentCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Document
    $scope.document = null;
    $scope.getDocument = function () {
      $http({
          method: 'GET',
          url: apiEndpoint + '/Documents/' + $stateParams.id
        })
        .then(function mySuccess(response) {
          $loading.hide();
          $scope.document = response.data;

          // Check Verified
          if (!response.data.verified) $scope.showVerify();
        }, function myError(err) {
          console.log(err);
          $loading.hide();
        });
    };
    $scope.getDocument();

    // Remove Document
    $scope.remove = function () {
      $loading.show('Loading', false);

      $http({
          method: 'DELETE',
          url: apiEndpoint + '/Documents/' + $stateParams.id
        })
        .then(function mySuccess(response) {
          $loading.hide();
          $state.go('app.dashboard.documents', null, {
            location: 'replace',
            reload: true
          });
        }, function myError(err) {
          console.log(err);
          $loading.hide();
        });
    };

    // Show Verify
    $scope.showVerify = function (ev) {
      $mdDialog.show({
        contentElement: '#verifyDialog',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      });
    };

    // Hide Verify
    $scope.hideVerify = function () {
      $mdDialog.hide();
    };
  });