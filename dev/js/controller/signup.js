'use strict';
angular.module(_CONTROLLERS_)

  .controller('SignupCtrl', function ($scope, $location, $loading, authService) {
    if (_DEBUG_) console.log('SignupCtrl ====');

    // We will store all of our form data in this object
    $scope.userData = {
      //mobile: 971566524933,
      //fname: 'Muhammad',
      //lname: 'Samir',
      //password: '1234'
    };

    // Signup
    $scope.signup = function (userData) {
      $loading.show('Loading', false);

      authService.register(userData)
        .then(function (response) {
          $loading.hide();

          //$location.path('/signin');
          $scope.signin(userData.mobile, userData.password)
          console.log(response);
        }, function (err) {
          $loading.hide();

          alert(err.data.error.message);
          console.log(err);
        });
    };

    // Signin
    $scope.signin = function (username, password) {
      $loading.show('Loading', false);

      authService.login(username, password)
        .then(function (response) {
          $loading.hide();

          $location.path('/dashboard');
          console.log(response);
        }, function (err) {
          $loading.hide();
          
          alert(err.data.error.message);
          console.log(err);
        });
    };
  });