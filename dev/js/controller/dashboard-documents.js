'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardDocumentsCtrl', function ($scope, $state, $timeout, $http, $loading, apiEndpoint, authService) {
    if (_DEBUG_) console.log('DashboardDocumentsCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Get Customer
    authService.getCurrentCustomer()
      .then(function (customer) {
        // Documents
        $scope.documents = [];
        $http({
            method: 'GET',
            url: apiEndpoint + '/Customers/' + customer.id + '/documents'
          })
          .then(function mySuccess(response) {
            $loading.hide();
            $scope.documents = response.data;
          }, function myError(err) {
            $loading.hide();
            console.log(err);
          });
      });
  });