'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardProductsCtrl', function ($scope, $state, $timeout, $http, $loading, apiEndpoint, authService) {
    if (_DEBUG_) console.log('DashboardProductsCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Filter
    $scope.filters = [{
      label: 'New'
    }];

    // Products
    $scope.products = [];
    $http({
        method: 'GET',
        url: apiEndpoint + '/Products',
        params: {
          filter: {
            where: {
              store: true
            }
          }
        }
      })
      .then(function mySuccess(response) {
        $loading.hide();
        $scope.products = response.data;
      }, function myError(err) {
        $loading.hide();
        console.log(err);
      });
  });