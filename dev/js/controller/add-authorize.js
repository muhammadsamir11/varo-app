'use strict';
angular.module(_CONTROLLERS_)

  .controller('AddAuthorizeCtrl', function ($scope, $state, $stateParams, $http, $loading, authService, apiEndpoint) {
    if (_DEBUG_) console.log('AddAuthorizeCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Init
    $scope.code = null;
    $scope.details = null;
    $http({
        method: 'GET',
        url: apiEndpoint + '/Codes/' + $stateParams.id
      })
      .then(function mySuccess(code) {
        $loading.hide();
        $scope.code = code.data;
        $http({
            method: 'GET',
            url: apiEndpoint + '/' + $scope.code.model + '/' + $scope.code.modelId
          })
          .then(function mySuccess(details) {
            $loading.hide();
            $scope.details = details.data;
          }, function myError(err) {
            console.log(err);
            $loading.hide();
          });
      }, function myError(err) {
        console.log(err);
        $loading.hide();
      });

    // Authorize
    $scope.authorize = function (code) {
      $loading.show('Loading', false);

      authService.getCurrentCustomer()
        .then(function (customer) {
          $http({
              method: 'PUT',
              url: apiEndpoint + '/Codes/' + $stateParams.id,
              data: {
                customerId: customer.id,
                requesterId: code.requesterId,
                model: code.model,
                modelId: code.modelId
              }
            })
            .then(function mySuccess(response) {
              if (code.model === 'Agents') {
                $loading.hide();
                $state.go('app.dashboard.home', null, {
                  location: 'replace',
                  reload: true
                });
              }

              if (code.model === 'Products') {
                $http({
                    method: 'POST',
                    url: apiEndpoint + '/Requests',
                    data: {
                      customerId: customer.id,
                      codeId: code.id,
                      companyId: code.requesterId,
                      productId: code.modelId
                    }
                  })
                  .then(function mySuccess(response) {
                    $loading.hide();
                    $state.go('app.dashboard.home', null, {
                      location: 'replace',
                      reload: true
                    });
                  }, function myError(err) {
                    console.log(err);
                    $loading.hide();
                  });
              }
            }, function myError(err) {
              console.log(err);
              $loading.hide();
            });
        });
    };
  });