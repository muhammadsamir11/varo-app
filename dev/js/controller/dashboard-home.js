'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardHomeCtrl', function ($scope, $timeout, $http, $loading, authService, apiEndpoint) {
    if (_DEBUG_) console.log('DashboardHomeCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Score
    $scope.score = 0;

    // Get Customer
    authService.getCurrentCustomer()
      .then(function (customer) {
        // Requests
        $scope.requests = [];
        $http({
            method: 'GET',
            url: apiEndpoint + '/Customers/' + customer.id + '/requests',
            params: {
              filter: {
                limit: 5
              }
            }
          })
          .then(function mySuccess(response) {
            //$loading.hide();
            $scope.requests = response.data;
          }, function myError(err) {
            console.log(err);
            $loading.hide();
          });

        // Documents
        $scope.documents = [];
        $http({
            method: 'GET',
            url: apiEndpoint + '/Customers/' + customer.id + '/documents',
            params: {
              filter: {
                limit: 5
              }
            }
          })
          .then(function mySuccess(response) {
            $loading.hide();
            $scope.documents = response.data;

            // Score
            if (response.data.length) {
              var total = 0;
              angular.forEach(response.data, function (value, key) {
                console.log(value.score);
                total += value.score;
              });
              $scope.score = total / response.data.length;
            }

          }, function myError(err) {
            console.log(err);
            $loading.hide();
          });
      });
  });