'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardRequestsCtrl', function ($scope, $state, $timeout, $http, $loading, apiEndpoint, authService) {
    if (_DEBUG_) console.log('DashboardRequestsCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Get Customer
    authService.getCurrentCustomer()
      .then(function (customer) {
        // Requests
        $scope.requests = [];
        $http({
            method: 'GET',
            url: apiEndpoint + '/Customers/' + customer.id + '/requests'
          })
          .then(function mySuccess(response) {
            $loading.hide();
            $scope.requests = response.data;
          }, function myError(err) {
            console.log(err);
            $loading.hide();
          });
      });
  });