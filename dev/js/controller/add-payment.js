'use strict';
angular.module(_CONTROLLERS_)

  .controller('AddPaymentCtrl', function ($scope, $state, $stateParams, $http, $loading, authService, apiEndpoint) {
    if (_DEBUG_) console.log('AddPaymentCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Request
    $scope.request = null;
    $http({
        method: 'GET',
        url: apiEndpoint + '/Requests/' + $stateParams.id
      })
      .then(function mySuccess(response) {
          $loading.hide();
          $scope.request = response.data;
        },
        function myError(err) {
          console.log(err);
          $loading.hide();
        });

    // Card Details
    $scope.card = {
      number: null,
      name: null,
      expiry: null,
      cvv: null
    };

    // Scan Card
    $scope.scanCard = function () {
      if (_IS_MOBILE_) {
        CardIO.canScan(function (canScan) {
          CardIO.scan({
            "requireExpiry": false,
            "scanExpiry": true,
            "requirePostalCode": false,
            "restrictPostalCodeToNumericOnly": false,
            "hideCardIOLogo": true,
            "suppressScan": false,
            "keepApplicationTheme": true
          }, function (card) {
            console.log(card['cardNumber']);
            $scope.card.number = parseInt(card['cardNumber']);
            console.log($scope.card);
            $scope.$apply();
          }, function () {
            console.log('Card cancel');
          });
        });
      } else {
        $scope.card.number = 1234;
      }
    };

    // Pay
    $scope.pay = function () {
      $loading.show('Loading', false);

      $http({
          method: 'PATCH',
          url: apiEndpoint + '/Requests/' + $stateParams.id,
          data: {
            payment: true
          }
        })
        .then(function mySuccess(response) {
          $loading.hide();
          $state.go('app.dashboard.home', null, {
            location: 'replace',
            reload: true
          });
        }, function myError(err) {
          console.log(err);
          $loading.hide();
        });
    };
  });