'use strict';
angular.module(_CONTROLLERS_)

  .controller('SignupOTPCtrl', function ($scope, $state, $timeout, $loading) {
    if (_DEBUG_) console.log('SignupOTPCtrl ====');

    $scope.verify = function (otp) {
      $loading.show('Loading', false);
      $timeout(function () {
        $loading.hide();
        $state.go('app.signup.name');
      }, 1000);
    };
  });