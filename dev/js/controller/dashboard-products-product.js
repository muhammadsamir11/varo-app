'use strict';
angular.module(_CONTROLLERS_)

  .controller('DashboardProductCtrl', function ($scope, $state, $stateParams, $timeout, $http, $loading, $mdDialog, apiEndpoint, authService) {
    if (_DEBUG_) console.log('DashboardProductCtrl ====');

    // Loading
    $loading.show('Loading', false);

    // Product
    $scope.product = null;
    $scope.getProduct = function () {
      $http({
          method: 'GET',
          url: apiEndpoint + '/Products/' + $stateParams.id
        })
        .then(function mySuccess(response) {
          $loading.hide();
          $scope.product = response.data;
        }, function myError(err) {
          console.log(err);
          $loading.hide();
        });
    };
    $scope.getProduct();
  });