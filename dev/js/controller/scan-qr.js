'use strict';
angular.module(_CONTROLLERS_)

  .controller('ScanQRCtrl', function ($scope, $state, $cordovaQr) {
    if (_DEBUG_) console.log('ScanQRCtrl ====');

    $cordovaQr.show();

    $cordovaQr.scan()
      .then(function (result) {
        console.log(JSON.parse(result));

        $state.go('app.add.authorize', JSON.parse(result), {
          location: 'replace',
          reload: true
        });
      })
      .catch(function (err) {
        console.log(err);
      });

    $scope.$on("$destroy", function () {
      $cordovaQr.destroy();
    });
  });