'use strict';
angular.module(_CONTROLLERS_)

  .controller('SignupMobileCtrl', function ($scope, $state, $timeout, $loading) {
    if (_DEBUG_) console.log('SignupMobileCtrl ====');
    
    $scope.next = function (mobile) {
      $loading.show('Loading', false);
      $timeout(function () {
        $loading.hide();
        $state.go('app.signup.otp');
      }, 1000);
    };
  });