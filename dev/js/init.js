var _APP_ = 'varo',
  _CONFIG_ = _APP_ + '.config',
  _CONTROLLERS_ = _APP_ + '.controllers',
  _DIRECTIVES_ = _APP_ + '.directives',
  _FILTERS_ = _APP_ + '.filters',
  _ROUTES_ = _APP_ + '.routes',
  _SERVICES_ = _APP_ + '.services',
  _DEBUG_ = true,
  _IN_BACKGROUND_ = false,
  _IS_MOBILE_,
  _DEVICE_TYPE_ = (navigator.userAgent.match(/iPad/i)) == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i)) == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";

angular.module(_APP_, [
    _CONFIG_,
    _CONTROLLERS_,
    _DIRECTIVES_,
    _FILTERS_,
    _ROUTES_,
    _SERVICES_,
    'ui.router',
    //'ngTouch',
    'ngAnimate',
    'ngAria',
    'ngMessages',
    'ngSanitize',
    'ngMaterial',
    'ngCookies',
    'ngResource',
    'ngCordova',
    'ksSwiper',
    'monospaced.qrcode',
    'ngMap'
  ])

  .run(function ($log, $rootScope, $state, $transitions, $location, authService) {
    // Hackers gotta hack
    $log.debug('Like inspecting code? :)');

    // Redirect to login page if not logged in and trying to access a restricted page
    // $rootScope.$on('$locationChangeStart', function (event, next, current) {
    //   //var restrictedPage = ($location.path() === '/signin') || ($location.path() === '/signup');
    //   if ($location.path() === '/dashboard' && !authService.isAuthenticated()) {
    //     $location.path('/signin');
    //   }
    //   if (authService.isAuthenticated()) {
    //     $location.path('/dashboard');
    //   }
    // });

    // Back button
    document.addEventListener('deviceready', function () {
      document.addEventListener('backbutton', function (event) {
        event.preventDefault();
        event.stopPropagation();
        if ($state.current.name === 'app.signin' || $state.current.name === 'app.dashboard.home') {
          navigator.app.exitApp();
        } else {
          navigator.app.backHistory();
        }
      }, false);
    }, false);
  })
  .config(function ($mdGestureProvider, $mdThemingProvider) {
    $mdGestureProvider.skipClickHijack();
    $mdThemingProvider.disableTheming();
  })
  .config(function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
  })
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  });

// Create global modules. You shouldn't have to touch these.
angular.module(_CONFIG_, []);
angular.module(_CONTROLLERS_, []);
angular.module(_DIRECTIVES_, []);
angular.module(_FILTERS_, []);
angular.module(_ROUTES_, []);
angular.module(_SERVICES_, []);