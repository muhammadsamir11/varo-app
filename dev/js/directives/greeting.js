'use strict';
angular.module(_DIRECTIVES_)

  .directive('greeting', function () {
    return function (scope, elm) {
      var hour = new Date().getHours();
      var greeting = 'Hello';
      if (hour > 4 && hour < 11) {
        greeting = 'Good Morning';
      } else if (hour > 18 && hour < 23) {
        greeting = 'Good evening';
      } else if (hour >= 23 || hour < 4) {
        greeting = 'Good night';
      }
      elm.text(greeting);
    };
  });