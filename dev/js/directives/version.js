'use strict';
angular.module(_DIRECTIVES_)

	.directive('version', function (version) {
		return function (scope, elm) {
			elm.text('version ' + version);
		};
	});