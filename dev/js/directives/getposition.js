'use strict';
angular.module(_DIRECTIVES_)

  .directive('getPosition', function () {
    return {
      restrict: 'A',
      scope: false,
      link: function (scope, element, attrs) {
        var rect = angular.element(element)[0].getBoundingClientRect();
        scope.rect = {
          x: rect.left / document.body.clientWidth,
          y: (rect.top - (window.screen.height - document.body.clientHeight + 4)) / document.body.clientHeight,
          width: rect.width / document.body.clientWidth,
          height: rect.height / document.body.clientHeight
        };
      }
    }
  });