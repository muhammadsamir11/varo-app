'use strict';
angular.module(_DIRECTIVES_)

    .directive('scrollToEnd', function ($timeout, $window) {
        return {
            restrict: 'AE',
            transclude: false,
            replace: false,
            scope: false,
            link: function (scope, element, attrs) {
                var scrolldiv = element[0];

                scrolldiv.scrollTop = scrolldiv.scrollHeight;
                scope.$watch('chatList', function (newValue, oldValue) {
                    $timeout(function () {
                        scrolldiv.scrollTop = scrolldiv.scrollHeight;
                    }, 10);
                }, true);
                angular.element($window).on('resize', function () {
                    if (_DEBUG_) console.log('resize', scrolldiv.scrollHeight);
                    $timeout(function () {
                        scrolldiv.scrollTop = scrolldiv.scrollHeight;
                    }, 10);
                });

                // On destroy
                /*$scope.$on('$destroy', function () {
                		angular.element($window).off('resize');
                });*/
            }
        };
    })

    .directive('scrollToStart', function ($timeout, $window) {
        return {
            restrict: 'AE',
            transclude: false,
            replace: false,
            scope: false,
            link: function (scope, element, attrs) {
                var scrolldiv = element[0];
                var count = 0;

                element.bind('scroll', function () {
                    if (scrolldiv.scrollTop === 0) {
                        count++;
                        if (count > 1) {
                            count = 0;
                            scope.content = false;
                            scope.$apply();
                        }
                    }
                });

                scope.$watch('content', function (newValue, oldValue, scope) {
                    if (!newValue) scrolldiv.scrollTop = 0;
                });
            }
        };
    });