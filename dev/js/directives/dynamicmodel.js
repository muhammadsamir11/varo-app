'use strict';
angular.module(_DIRECTIVES_)

    .directive('dynamicModel', function ($compile, $parse) {
        return {
            restrict: 'A',
            terminal: true,
            priority: 100000,
            link: function (scope, elem) {
                var name = $parse(elem.attr('dynamic-model'))(scope);
                elem.removeAttr('dynamic-model');
                elem.attr('ng-model', name);
                $compile(elem)(scope);
            }
        };
    });