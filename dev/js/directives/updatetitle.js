'use strict';
angular.module(_DIRECTIVES_)

    .directive('updateTitle', function ($state, $transitions, $timeout) {
        return {
            restrict: 'AE',
            transclude: true,
            replace: true,
            scope: {
                tasks: '='
            },
            link: function (scope, element) {
                var title;

                $timeout(function () {
                    if ($state.current.data && $state.current.data.title) {
                        title = $state.current.data.title;
                        element.text(title);
                    }
                }, 0, false);

                $transitions.onSuccess({
                    to: true
                }, function (transition) {
                    if (transition.router.stateService.current.data && transition.router.stateService.current.data.title) {
                        title = transition.router.stateService.current.data.title;
                        element.text(title);
                    } else {
                        element.text('');
                    }
                });
            }
        };
    });