'use strict';
angular.module(_ROUTES_, ['ui.router'])

  .constant('ROUTES', {
    'app': {
      url: '',
      views: {
        "@": {
          templateUrl: 'templates/layout.html',
          controller: 'MainCtrl'
        }
      }
    },

    'app.signin': {
      url: '/signin',
      views: {
        'page@app': {
          templateUrl: 'partials/signin.html',
          controller: 'SigninCtrl'
        }
      },
      data: {
        title: 'Signin',
        authRequired: false
      }
    },
    'app.signup': {
      abstract: true,
      url: '/signup',
      views: {
        'page@app': {
          templateUrl: 'partials/signup.html',
          controller: 'SignupCtrl'
        }
      },
      data: {
        title: 'Signup',
        authRequired: false
      }
    },
    'app.signup.type': {
      url: '',
      views: {
        'content@app.signup': {
          templateUrl: 'partials/signup-type.html'
          //controller: 'SignupTypeCtrl'
        }
      }
    },
    'app.signup.mobile': {
      url: '/mobile',
      views: {
        'content@app.signup': {
          templateUrl: 'partials/signup-mobile.html',
          controller: 'SignupMobileCtrl'
        }
      }
    },
    'app.signup.otp': {
      url: '/otp',
      views: {
        'content@app.signup': {
          templateUrl: 'partials/signup-otp.html',
          controller: 'SignupOTPCtrl'
        }
      }
    },
    'app.signup.name': {
      url: '/name',
      views: {
        'content@app.signup': {
          templateUrl: 'partials/signup-name.html',
          //controller: 'SignupNameCtrl'
        }
      }
    },
    'app.signup.face': {
      url: '/face',
      views: {
        'content@app.signup': {
          templateUrl: 'partials/signup-face.html',
          controller: 'SignupFaceCtrl'
        }
      }
    },
    'app.signup.password': {
      url: '/password',
      views: {
        'content@app.signup': {
          templateUrl: 'partials/signup-password.html'
        }
      }
    },


    'app.dashboard': {
      abstract: true,
      url: '/dashboard',
      views: {
        'page@app': {
          templateUrl: 'partials/dashboard.html',
          controller: 'DashboardCtrl'
        }
      }
    },
    'app.dashboard.home': {
      url: '',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-home.html',
          controller: 'DashboardHomeCtrl'
        }
      },
      data: {
        title: 'Dashboard',
        tabBar: true,
        authRequired: true
      }
    },
    'app.dashboard.documents': {
      url: '/documents',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-documents.html',
          controller: 'DashboardDocumentsCtrl'
        }
      },
      data: {
        title: 'Documents',
        tabBar: true,
        authRequired: true
      }
    },
    'app.dashboard.documents.document': {
      url: '/:id',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-documents-document.html',
          controller: 'DashboardDocumentCtrl'
        }
      },
      data: {
        title: 'Document',
        tabBar: false,
        authRequired: true
      }
    },
    'app.dashboard.requests': {
      url: '/requests',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-requests.html',
          controller: 'DashboardRequestsCtrl'
        }
      },
      data: {
        title: 'Requests',
        tabBar: true,
        authRequired: true
      }
    },
    'app.dashboard.requests.request': {
      url: '/:id',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-requests-request.html',
          controller: 'DashboardRequestCtrl'
        }
      },
      data: {
        title: 'Request',
        tabBar: false,
        authRequired: true
      }
    },
    'app.dashboard.products': {
      url: '/products',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-products.html',
          controller: 'DashboardProductsCtrl'
        }
      },
      data: {
        title: 'Products',
        tabBar: true,
        authRequired: true
      }
    },
    'app.dashboard.products.product': {
      url: '/:id',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-products-product.html',
          controller: 'DashboardProductCtrl'
        }
      },
      data: {
        title: 'Product',
        tabBar: false,
        authRequired: true
      }
    },
    'app.dashboard.settings': {
      url: '/settings',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-settings.html',
          controller: 'DashboardSettingsCtrl',
        }
      },
      data: {
        title: 'Settings',
        tabBar: false,
        authRequired: true
      }
    },
    'app.dashboard.locations': {
      url: '/locations',
      views: {
        'content@app.dashboard': {
          templateUrl: 'partials/dashboard-locations.html',
          controller: 'DashboardLocationsCtrl',
        }
      },
      data: {
        title: 'Map',
        tabBar: false,
        authRequired: true
      }
    },
    

    'app.add': {
      abstract: true,
      url: '/add',
      views: {
        'page@app': {
          templateUrl: 'partials/add.html',
          controller: 'AddCtrl'
        }
      },
      data: {
        title: 'Add Diamond',
        authRequired: false
      }
    },
    'app.add.options': {
      url: '',
      views: {
        'content@app.add': {
          templateUrl: 'partials/add-options.html',
          //controller: 'AddOptionsCtrl'
        }
      }
    },
    'app.add.types': {
      url: '/types',
      views: {
        'content@app.add': {
          templateUrl: 'partials/add-types.html',
          controller: 'AddTypesCtrl'
        }
      }
    },
    'app.add.document': {
      url: '/document/:id',
      views: {
        'content@app.add': {
          templateUrl: 'partials/add-document.html',
          controller: 'AddDocumentCtrl'
        }
      }
    },
    'app.add.authorize': {
      //url: '/authorize/:id?type',
      url: '/authorize/:id',
      views: {
        'content@app.add': {
          templateUrl: 'partials/add-authorize.html',
          controller: 'AddAuthorizeCtrl'
        }
      }
    },
    'app.add.payment': {
      url: '/payment/:id',
      views: {
        'content@app.add': {
          templateUrl: 'partials/add-payment.html',
          controller: 'AddPaymentCtrl'
        }
      }
    }, 
    

    'app.scan': {
      abstract: true,
      url: '/scan',
      views: {
        'page@app': {
          templateUrl: 'partials/scan.html',
          controller: 'ScanCtrl'
        }
      },
      data: {
        title: 'Scan',
        authRequired: false
      }
    },
    'app.scan.qr': {
      url: '',
      views: {
        'content@app.scan': {
          templateUrl: 'partials/scan-qr.html',
          controller: 'ScanQRCtrl'
        }
      }
    },


    'app.404': {
      url: '/404',
      views: {
        'page@app': {
          templateUrl: 'partials/404.html',
          controller: 'NotFoundCtrl'
        }
      },
      data: {
        title: '404',
        authRequired: false
      }
    }
  })


  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'ROUTES', function ($stateProvider, $urlRouterProvider, $locationProvider, ROUTES) {
    // the route people are sent to when they are lost
    // the home page in this case
    $urlRouterProvider.when('', '/signin');
    $urlRouterProvider.otherwise('/404');

    // Use instead
    // $urlRouterProvider.otherwise(function ($injector) {
    //     var $state = $injector.get("$state");
    //     $state.go('/somestate');
    // });

    // create our routes, set the view, pull in the controller
    angular.forEach(ROUTES, function (route, path) {
      $stateProvider.state(path, route);
    });
  }]);