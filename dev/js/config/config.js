'use strict';
angular.module(_CONFIG_, ['ui.router'])

  .constant('version', 'version_num')
  //.constant('apiEndpoint', 'http://localhost:3000/api')
  .constant('apiEndpoint', 'https://varo-blockchain.herokuapp.com/api')
  .constant('faceApi', {
    'endpoint': 'https://westeurope.api.cognitive.microsoft.com/face/v1.0',
    'key': '34155df95dec40ec8a93d20c8b290413',
    'group': 'varo'
  });